$(document).ready(function () {
    $(window).click(function () {
        $('.search-box-mb').removeClass('active');
    });
    if ($(window).width() > 768) {
        $('.news-search').elementFixScroll('searchAffix');
    }
    if ($('#home-slide')) {
        $('#home-slide').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            prevArrow: "<button class=\"slide-prev\"><i class=\"icon-ic-arrow-left\"></i></button>",
            nextArrow: "<button class=\"slide-next\"><i class=\"icon-ic-arrow-right\"></i></button>"
        });
    }
    
    if($('.nav-bar--select')) {
        $('.nav-bar--select').click(function () {
            $(this).parent().find('.nav-bar--inner').addClass('active');
        })
        $('.nav-bar--inner').click(function () {
            $(this).removeClass('active');
        })
        $('.nav-bar--items').click(function (e) {
            e.stopPropagation();
        })
        $('.nav-bar--close').click(function (e) {
            e.stopPropagation();
            $(this).closest('.nav-bar--inner').removeClass('active');
        })
    }
    if ($('.y-collapse')) {
        $('.y-collapse-title').click(function () {
            $(this).parent().toggleClass('open');
        })
    }

    if ($('.tab-bar-item')) {
        $('.tab-bar-item').click(function () {
            $(this).siblings().removeClass('active');
            $(this).addClass('active');
            var tabContentActive = $(this).attr('data-content');
            if ($(tabContentActive)) {
                $(tabContentActive).siblings().removeClass('active');
                $(tabContentActive).addClass('active')
            }
        })
    }

    if ($('.lang')) {
        $('.lang').click(function () {
            $('.lang-select').toggleClass('show');
        })
        $('.lang-select .lang-option').click(function (e) {
            e.stopPropagation();
            $('.lang-select').removeClass('show');
            var lag = $(this).attr('data-lang');
            $('.lang>img').attr('src', 'img/' + lag + '.png')
        })
    }

    $('.search-box-mb').click(function (e) {
        e.stopPropagation();
        $(this).addClass('active');
    })

    $('.menu-button').click(function (e) {
        $(this).toggleClass('active');
        $('body').toggleClass('no-scroll');
        $('#menu-mb').toggleClass('menu-mobile--open');
    })

    $('.menu-item .menu-item-btn').click(function (e) {
        e.preventDefault();
        $menuItem = $(this).closest('.menu-item');
        $menuItem.siblings().removeClass('active');
        $menuItem.toggleClass('active');
    })

    var positionSticky = $('.menu').position().top;
    var positionStickyMb = $('header').position().top;
    var scroll_h = $(window).scrollTop();
    var window_w = $(window).width();

    if (window_w < 960) {
        var offsetTopHeader = $('header').offset().top;
        if (scroll_h >= positionSticky && offsetTopHeader != 0) {
            $('header').addClass('sticky');
        }
    } else {
        var offsetTopMenu = $('.menu').offset().top;
        if (scroll_h >= positionSticky && offsetTopMenu != 0) {
            $('.menu').addClass('sticky');
        }
    }
    $(window).scroll(function (e) {
        if (window_w < 960) {
            scroll_h = $(window).scrollTop();
            if (scroll_h > positionStickyMb) {
                $('header').addClass('sticky');
            } else {
                $('header').removeClass('sticky');
            }
        } else {
            scroll_h = $(window).scrollTop();
            if (scroll_h > positionSticky) {
                $('.menu').addClass('sticky');
            } else {
                $('.menu').removeClass('sticky');
            }
        }

        if (scroll_h > $(this).outerHeight()) {
            $('.btn-back-to-top').addClass('show')
        } else {
            $('.btn-back-to-top').removeClass('show');
        }
    })

    $('.btn-back-to-top').click(function () {
        $('html').animate({
            scrollTop: 0
        }, 500)
    })
})

jQuery.fn.extend({
    scrollTo: function (element) {
        var element = $(element);
        $(this).click(function () {
            $(this).parents().find('.menu__tabs a').removeClass('active');
            $(this).find('a').addClass('active');

            $('html').animate({
                scrollTop: element.offset().top - 100
            }, 1000)
        })
    },

    countdown: function (timestamp) {
        var e = $(this);
        var now = new Date().getTime();
        var x = setInterval(function () {
            now = now + 1000;
            // Find the distance between now and the count down date
            var distance = timestamp - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (seconds < 0 && minutes < 0 && hours < 0 && days < 0) {
                days = hours = minutes = seconds = "00"
                clearInterval(x);
            }
            if (days.toString().length == 1) {
                days = "0" + days;
            }
            if (hours.toString().length == 1) {
                hours = "0" + hours;
            }
            if (minutes.toString().length == 1) {
                minutes = "0" + minutes;
            }
            if (seconds.toString().length == 1) {
                seconds = "0" + seconds;
            }
            e.find('.dd').html(days);
            e.find('.hh').html(hours);
            e.find('.mm').html(minutes);
            e.find('.ss').html(seconds);

        }, 1000);
    },

    addAnimate: function (array) {
        var wh = $(window).height();
        var add_class_name = "animated fadeInUp";
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            for (var element in array) {
                add_class_name = "animated " + array[element];
                if (scroll > $(element).position().top - wh) {
                    $(element).addClass(add_class_name);
                }
            }
        });
    },

    createSlider: function (itemShow, itemShowMb) {
        $(this).slick({
            infinite: true,
            slidesToShow: itemShow,
            slidesToScroll: 1,
            dots: true,
            prevArrow: '<button type="button" class="btn btn--white btn--circle btn--shadow slick-prev"><i class="yinicon-arrow-left"></i></button>',
            nextArrow: '<button type="button" class="btn btn--white btn--circle btn--shadow slick-next"><i class="yinicon-arrow-right"></i></button>',
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: itemShow - 1,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: itemShow - 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: itemShowMb,
                        slidesToScroll: itemShowMb
                    }
                }
            ]
        });
    },

    elementFixScroll: function (class_name) {
        var _this = $(this)
        if (_this.length) {
            var position = _this.position().top;
            var offsetTop = _this.offset().top;
            var scroll_h = $(window).scrollTop();
            if (scroll_h >= position && offsetTop != 0) {
                _this.addClass(class_name);
            }
            $(window).scroll(function () {
                scroll_h = $(window).scrollTop();
                if (scroll_h > position) {
                    _this.addClass(class_name);
                }
                else {
                    _this.removeClass(class_name);
                }
            })
        }
    }
})



